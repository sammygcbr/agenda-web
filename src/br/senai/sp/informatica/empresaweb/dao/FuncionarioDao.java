package br.senai.sp.informatica.empresaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.empresaweb.model.Funcionario;

public class FuncionarioDao {

	//atributo
	Connection connection;
	
	//construtor
	public FuncionarioDao() {
		this.connection = new ConnectionFactory().getConnection();
	}
	
	// metodo salva
	public void salva(Funcionario funcionario) {

		// comando sql
		String sql = "INSERT INTO funcionario (nome, email, cpf, senha) VALUES(?, ?, ?, ?)";

		try {
			// cria um PreparedStatement com o comando sql
			// java.sql.PreparedStatement
			PreparedStatement stmt = connection.prepareStatement(sql);

			// cria os parametros para as ?
			stmt.setString(1, funcionario.getNome());
			stmt.setString(2, funcionario.getEmail());
			stmt.setString(3, funcionario.getCpf());
			stmt.setString(4, funcionario.getSenha());

			stmt.execute();

			// libera o recurso de prepared statement
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	// metodo getLista
		public List<Funcionario> getLista() {

			try {
				// cria uma lista de contatos
				List<Funcionario> funcionarios = new ArrayList<>();

				// cria um PreparedStatement
				PreparedStatement stmt = connection.prepareStatement("SELECT * FROM funcionario");

				// guarda os resultados da consulta em um ResultSet
				ResultSet rs = stmt.executeQuery();

				// enquanto houver um proximo registro no resultSet
				while (rs.next()) {
					// cria um nome contato com os dados do resultSet
					Funcionario funcionario = new Funcionario();
					funcionario.setId(rs.getLong("id"));
					funcionario.setNome(rs.getString("nome"));
					funcionario.setEmail(rs.getString("email"));
					funcionario.setCpf(rs.getString("cpf"));
					funcionario.setSenha(rs.getString("senha"));


					// adiciona o contato ao arraylist contatos
					funcionarios.add(funcionario);
				}

				// libera o recurso de resultSet
				rs.close();

				// libera o PreparedStatement
				stmt.close();

				// retorna a lsita de conatos
				return funcionarios;
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
}
