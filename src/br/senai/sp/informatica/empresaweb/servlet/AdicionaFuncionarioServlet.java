package br.senai.sp.informatica.empresaweb.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;
import br.senai.sp.informatica.empresaweb.model.Funcionario;

@WebServlet("/adicionaFuncionario")
public class AdicionaFuncionarioServlet extends HttpServlet {

	//sobrescreve o m�todo service
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// obt�m um PintWriter
		// esse objeto nos permitira escrever mensagens no response
		PrintWriter out = res.getWriter();

		// pega os parametros da requisi��o por meio dos nomes do form
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String cpf = req.getParameter("cpf");
		String senha = req.getParameter("senha");
		
		//cria uma instancia nova de contato
		Funcionario funcionario = new Funcionario();
		//define os atributos do contato
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setCpf(cpf);
		funcionario.setSenha(senha);
		
		//obtem uma instancia de dao
		FuncionarioDao dao = new FuncionarioDao();
		
		//salva o contato
		dao.salva(funcionario);
		
		//devolve uma resposta ao usuario
		out.println("<html>");
		out.println("<body>");
		out.println("Funcionario " + funcionario.getNome() + " salvo com sucesso!");
		out.println("</body>");
		out.println("</html>");
	}
}
